A slightly modified client to go with the slightly modified server. Also in the 1.4 format needed for mesh. Slightly modified from a project by Magnie on scratch.mit.edu, to use an older format, include credit, and disable the /;ascratch command.
http://scratch.mit.edu/projects/2440837/ Used under CC-BY-SA 2.0 License. This project is under the same license.
http://creativecommons.org/licenses/by-sa/2.0/deed.en

Server available at https://bitbucket.org/zippynk/chat.py-2.0-server-modified-code under a different license not having to do with the one used for this project.

To use, open in a copy of Scratch 1.4 and follow the onscreen instructions.